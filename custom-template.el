;;; custom.el --- user customization file

;;; Commentary:
;;;       Add or change the configurations in custom.el, then restart Emacs.
;;;       

;;; Code:
;;; (setq Libra-tabbar t)         ; Enable Tabbar

;; Misc.
;; (setq confirm-kill-emacs 'y-or-n-p)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;;; custom.el ends here
