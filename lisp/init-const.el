;;; init-const.el --- Define constants.

;;; Commentary:
;;; Syscont

;;; Code:

(defconst Libra-homepage
  "https://bitbucket.org/-0w0-/.emacs.d/src/master/")

(defconst sys/win32p
  (eq system-type 'windows-nt)
  "Are we running on a WinTel system?")

(defconst sys/linuxp
  (eq system-type 'gnu/linux)
  "Are we running on a GNU/Linux system?")

(defconst sys/linux-x-p
  (and (display-graphic-p) sys/linuxp)
  "Are we running under X on a GNU/Linux?")

(defconst sys/rootp
  (string-equal "root" (getenv "User"))
  "Are you using Root user?")

(defconst emacs/>=25p
  (>= emacs-major-version 25)
  "Emacs is 25 or above.")

(defconst emacs/>=26p
  (>= emacs-major-version 26)
  "Emacs is 26 or above.")

(defconst emacs/>=27p
  (>= emacs-major-version 27)
  "Emacs is 27 or above.")

(defconst emacs/>=25.3p
  (or emacs/>=26p
      (and (= emacs-major-version 25) (>= emacs-minor-version 3)))
  "Emacs is 25.3 or above.")

(defconst emacs/>=25.2p
  (or emacs/>=26p
      (and (= emacs-major-version 25) (>= emacs-minor-version 2)))
  "Emacs is 25.2 or above.")

(defconst libra-leader "SPC"
  "Libra Emacs leader key.")

(defconst libra-non-normal-leader "C-,"
  "Libra Emacs non normal state leader key.")

(defconst libra-input-states '(normal visual insert emacs)
  "Libra Emacs input states.")

(defconst libra-pyim-dicts-path user-emacs-directory
  "Libra Emacs pyim-dicts path.")

(provide 'init-const)

;;; init-const.el ends here
