;; init-ui.el --- Better lookings and appearances.

;;; Commentary:
;;
;; Visual (UI) configurations for better lookings and appearances.
;;

;;; Code:

(eval-when-compile
  (require 'init-custom))

(require 'init-func)

;; Menu/Tool/Scroll bars
(unless emacs/>=27p
  (push '(menu-bar-lines . 0) default-frame-alist)
  (push '(tool-bar-lines . 0) default-frame-alist)
  (push '(vertical-scroll-bars) default-frame-alist))

;; Dashboard
(use-package dashboard
  :init
  (dashboard-setup-startup-hook)
  :config
  (setq dashboard-banner-logo-title "Welcome to LibraEmacs")
  (setq dashboard-startup-banner "~/.emacs.d/image/logo/Libra.png")
  (setq dashboard-footer-messages '("Fiat justitia ruat caelum")))

;; Themes
(use-package doom-themes
  :defer
  :config
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  (load-theme 'doom-vibrant t))

(use-package apropospriate-theme :defer)

(use-package circadian
  :init
  (setq circadian-themes '(("8:00" . apropospriate-light)
                           ("18:30" . doom-vibrant)))
  (circadian-setup))

;; ModeLine
(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))

(use-package hide-mode-line
  :hook (((completion-list-mode completion-in-region-mode) . hide-mode-line-mode)))

;; A minor-mode menu for mode-line
(when emacs/>=25.2p
  (use-package minions
    :hook (doom-modeline-mode . minions-mode)))

;; Suppress GUI features
(setq use-file-dialog nil
      use-dialog-box nil
      inhibit-startup-screen t
      inhibit-startup-echo-area-message t)

;; Display Diveders between windows
(setq window-divider-default-places t
      window-divider-default-bottom-width 3
      window-divider-default-right-width 3)
(add-hook 'window-setup-hook #'window-divider-mode)

(when Libra-tabbar
  (use-package centaur-tabs
  :demand
  :config
  (centaur-tabs-mode t)
  (setq centaur-tabs-height 32)
  (setq centaur-tabs-set-icons t)
  (setq centaur-tabs-gray-out-icons 'buffer)
  (setq centaur-tabs-set-bar 'under)
  (setq x-underline-at-descent-line t)
  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward)
  :hook
  (dired-mode . centaur-tabs-local-mode)
  (dashboard-mode . centaur-tabs-local-mode)))

;; Fonts
(if sys/win32p (setq inhibit-compacting-font-caches t))

(when (display-graphic-p)
  ;; Set default font
  (cl-loop for font in '("JetBrains Mono" "SF Mono" "Hack" "Source Code Pro" "Fira Code"
                         "Menlo" "Monaco" "DejaVu Sans Mono" "Consolas")
           when (font-installed-p font)
           return (set-face-attribute 'default nil
                                      :font font
                                      :height (cond (sys/win32p 100)
                                                    (t 100))))

  ;; Specify font for all unicode characters
  (cl-loop for font in '("Symbola" "Apple Symbols" "Symbol" "icons-in-terminal")
           when (font-installed-p font)
           return (set-fontset-font t 'unicode font nil 'prepend))

  ;; Specify font for Chinese characters
  (cl-loop for font in '("WenQuanYi Micro Hei" "Microsoft Yahei")
           when (font-installed-p font)
           return (set-fontset-font t '(#x4e00 . #x9fff) font)))

;; cnfonts package
(use-package cnfonts
  :after all-the-icons
  :hook (cnfonts-set-font-finish
	 .(lambda (fontsizes-list)
	          (set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'append)
              (set-fontset-font t 'unicode (font-spec :family "file-icons") nil 'append)
              (set-fontset-font t 'unicode (font-spec :family "Material Icons") nil 'append)
              (set-fontset-font t 'unicode (font-spec :family "github-octicons") nil 'append)
              (set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'append)
              (set-fontset-font t 'unicode (font-spec :family "Weather Icons") nil 'append)))
  :config
  (cnfonts-enable)
  (setq cnfonts-use-system-type t))

;; Posframe
(use-package posframe)

(provide 'init-ui)

;;; init-ui.el ends here
