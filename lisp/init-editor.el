;;; init-editor.el --- Libra-Emacs Editor Configurations
;;; Commentary:
;;; Code:

(eval-when-compile
  (require 'init-custom))

(eval-when-compile
  (require 'init-func))

(use-package smartparens
  :hook
  (after-init . smartparens-global-mode)
  (rust-mode . (lambda () (require 'smartparens-rust)))
  :commands sp-pair sp-local-pair sp-with-modes sp-point-in-comment sp-point-in-string
  :config
  (require 'smartparens-config)
  (setq sp-highlight-pair-overlay nil
        sp-highlight-wrap-overlay nil
        sp-highlight-wrap-tag-overlay nil)
  (with-eval-after-load 'evil
    (setq sp-show-pair-from-inside t)
    (setq sp-cancel-autoskip-on-backward-movement nil)))

;;; Libra Emacs line number and current line.
(use-package nlinum-relative
  :init
  (setq nlinum-relative-current-symbol "->")
  (setq nlinum-relative-redisplay-delay 0)
  :config (nlinum-relative-setup-evil)
  :hook (prog-mode . nlinum-relative-mode))

;;; Save My Last Leave Line.
(use-package saveplace
  :hook (after-init . save-place-mode))

(use-package so-long
  :ensure nil
  :config
  (global-so-long-mode 1))

(use-package simple
  :ensure nil
  :hook (after-init . (lambda ()
                        (line-number-mode)
                        (column-number-mode)
                        (size-indication-mode))))

(use-package hideshow
  :ensure nil
  :diminish hs-minor-mode
  :bind (:map prog-mode-map
              ("C-c <" . hs-hide-block)
              ("C-c >" . hs-show-block))
  :hook (prog-mode . hs-minor-mode)
  :custom
  (hs-special-modes-alist
   (mapcar 'purecopy
           '((csharp-mode "\\(^[ \\t]*#[ \\t]*region\\b\\)\\|{" "\\(^[ \\t]*#[ \\t]*endregion\\b\\)\\|}"
                          "/[*/]"
                          csharp-hs-forward-sexp
                          hs-c-like-adjust-block-beginning)))))

(use-package auto-revert
  :ensure nil
  :hook (after-init . global-auto-revert-mode))

;; Default Input Method
(use-package pyim
  :demand t
  :config
  (use-package pyim-basedict
    :ensure
    :config (pyim-basedict-enable))
  (setq default-input-method 'pyim)
  (setq pyim-default-scheme 'quanpin)
  (setq-default pyim-english-input-switch-functions
              '(pyim-probe-dynamic-english
                pyim-probe-isearch-mode
                pyim-probe-program-mode
                pyim-probe-org-structure-template))
  (setq-default pyim-punctuation-half-width-functions
              '(pyim-probe-punctuation-line-beginning
                pyim-probe-punctuation-after-punctuation))
  (pyim-isearch-mode 1)
  (setq pyim-page-tooltip 'posframe
        pyim-page-length 5
        pyim-page-style 'vertical)
  :bind
  (("M-j" . pyim-convert-string-at-point)
   ("C-;" . pyim-delete-word-from-personal-buffer))
  :custom-face
  (pyim-page ((t (:inherit default :background "white" :foreground "black")))))

(provide 'init-editor)

;;; init-editor.el ends here
