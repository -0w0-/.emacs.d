;; init-rust.el --- Initialize Rust configurations.	-*- lexical-binding: t -*-

;;; Commentary:
;;
;; Rust configurations.
;;

;;; Code:

;; Rust
(use-package rust-mode
  :init (setq rust-format-on-save t)
  :config
  (use-package cargo
    :diminish cargo-minor-mode
    :hook (rust-mode . cargo-minor-mode)
    :config
    (setq compilation-filter-hook
          (append compilation-filter-hook '(cargo-process--add-errno-buttons)))))

(use-package rust-playground)

(provide 'init-rust)

;;; init-rust.el ends here
