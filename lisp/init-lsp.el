;;; init-lsp.el --- Initialize LSP configurations.

;;; Commentary:
;;; Language Server Protocol Configuration.
;;;
;;; Code:

(require 'init-func)

(use-package lsp-mode
  :diminish
  :hook ((prog-mode . (lambda ()
                           (unless (derived-mode-p 'emacs-lisp-mode 'lisp-mode 'python-mode)
                             (lsp-deferred))))
         (lsp-mode . (lambda ()
                       ;; Integrate `which-key'
                       (lsp-enable-which-key-integration)))
         (ruby-mode . (lambda ()
                        (require 'lsp-solargraph))))
  :init
  (setq lsp-prefer-capf t)
  (setq lsp-enable-text-document-color nil)
  :config
  ;; Code Hovers
  (use-package lsp-ui
    :config
    (advice-add 'keyboard-quit :before 'lsp-ui-doc-hide)
    (add-hook 'after-load-theme-hook
              (lambda ()
                (setq lsp-ui-doc-border (face-foreground 'default))
                (set-face-background 'lsp-ui-doc-background
                                     (face-background 'tooltip)))))

  ;; Ivy integration
  (use-package lsp-ivy
    :after lsp-mode
    :bind (:map lsp-mode-map
                ([remap xref-find-apropos] . lsp-ivy-workspace-symbol)
                ("C-s-." . lsp-ivy-global-workspace-symbol))))

;;(use-package nox
;;  :quelpa (nox :fetcher github :repo "manateelazycat/nox" :upgrade t))

(provide 'init-lsp)
;;; init-lsp.el ends here
