;; init-highlight.el --- Initialize highlighting configurations.

;;; Commentary:
;;;
;;; Highlight Configurations.
;;;

;;; Code:

(eval-when-compile
  (require 'init-const))

;; Highlight the current line
(use-package hl-line
  :hook (after-init . global-hl-line-mode))

;; Highlight matching parens
(use-package paren
  :hook (after-init . show-paren-mode)
  :config (setq show-paren-when-point-inside-paren t
		show-paren-when-point-in-periphery t))

(when (display-graphic-p)
  (use-package highlight-indent-guides
    :diminish
    :commands highlight-indent-guides--highlighter-default
    :hook (prog-mode . highlight-indent-guides-mode)
    :init (setq highlight-indent-guides-method 'character
		highlight-indent-guides-responsive 'top)
    :config
    ;; Dont' Display indentations while editing with 'company'
    (with-eval-after-load 'company
      (add-hook 'company-completion-started-hook
                (lambda (&rest _)
                  "Trun off indentation highlighting."
                  (when highlight-indent-guides-mode
                    (highlight-indent-guides-mode -1))))
      (add-hook 'company-after-completion-hook
                (lambda (&rest _)
                  "Trun on indentation highlighting."
                  (when (and (derived-mode-p 'prog-mode)
                             (not highlight-indent-guides-mode))
                    (highlight-indent-guides-mode 1)))))))

;; Colorize color names in buffers
(use-package rainbow-mode
  :diminish
  :functions (my-rainbow-colorize-match my-rainbow-clear-overlays)
  :commands (rainbow-x-color-luminance rainbow-colorize-match rainbow-turn-off)
  :bind (:map help-mode-map
         ("w" . rainbow-mode))
  :hook ((css-mode scss-mode less-css-mode) . rainbow-mode)
  :config
  ;; HACK: Use overlay instead of text properties to override `hl-line' faces.
  ;; @see https://emacs.stackexchange.com/questions/36420
  (defun my-rainbow-colorize-match (color &optional match)
    (let* ((match (or match 0))
           (ov (make-overlay (match-beginning match) (match-end match))))
      (overlay-put ov 'ovrainbow t)
      (overlay-put ov 'face `((:foreground ,(if (> 0.5 (rainbow-x-color-luminance color))
                                                "white" "black"))
                              (:background ,color)))))
  (advice-add #'rainbow-colorize-match :override #'my-rainbow-colorize-match)

  (defun my-rainbow-clear-overlays ()
    "Clear all rainbow overlays."
    (remove-overlays (point-min) (point-max) 'ovrainbow t))
  (advice-add #'rainbow-turn-off :after #'my-rainbow-clear-overlays))

;; Highlight brackets according to their depth
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(provide 'init-highlight)

;;; init-highlight.el ends here
