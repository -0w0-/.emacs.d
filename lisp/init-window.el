;; init-window.el --- Initialize window configurations.

;;; Commentary:
;;
;; Window configurations.
;;

;;; Code:


;; Restore olo window configurations
(use-package winner
  :commands (winner-undo winner-redo)
  :hook (after-init . winner-mode)
  :init (setq winner-boring-buffers '("*Completions*"
                                     "*Compile-Log*"
                                     "*inferior-lisp*"
                                     "*Fuzzy Completions*"
                                     "*Apropos*"
                                     "*Help*"
                                     "*cvs*"
                                     "*Buffer List*"
                                     "*Ibuffer*")))

(provide 'init-window)


;;; init-window.el ends here
