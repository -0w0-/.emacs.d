;; init-elpy.el --- Initialize elpy configurations.

;;; Commentary:
;;
;; Elpy Configurations.
;;

;;; Code:

(use-package elpy
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)
  :config
  (with-eval-after-load 'elpy
    (setq elpy-modules
	  (delete 'elpy-module-flymake elpy-modules)))
  (add-hook 'elpy-mode-hook 'flycheck-mode))


(provide 'init-elpy)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; init-elpy.el ends here
