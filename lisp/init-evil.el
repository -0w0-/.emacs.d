;;; init-evil --- Evil Configuration

;;; Commentary:
;;;
;;; Evil Configuration.
;;;

;;; Code:

(use-package evil
  :hook (after-init . evil-mode)
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil))

(use-package evil-collection
  :after evil
  :config
  ;; Utils mode
  (with-eval-after-load 'ibuffer (evil-collection-init 'ibuffer))
  (with-eval-after-load 'imenu-list (evil-collection-init 'imenu-list))
  (with-eval-after-load 'dired (evil-collection-init 'dired))
  (with-eval-after-load 'dired-sidebar (evil-collection-init 'dired-siderbr))
  (with-eval-after-load 'elfeed (evil-collection-init 'elfeed))
  (with-eval-after-load 'flycheck (evil-collection-init 'flycheck))
  (with-eval-after-load 'grep (evil-collection-init 'grep))
  (with-eval-after-load 'bookmark (evil-collection-init 'bookmark))
  (with-eval-after-load 'help (evil-collection-init 'help))
  (with-eval-after-load 'ivy (evil-collection-init 'ivy))
  (with-eval-after-load 'minibuffer (evil-collection-init 'minibuffer))
  
  ;; Language mode
  (with-eval-after-load 'emacs-lisp-mode (evil-collection-init 'elisp-mode))
  (with-eval-after-load 'ruby-mode (evil-collection-init 'ruby-mode))
  (with-eval-after-load 'python-mode (evil-collection-init 'python)))

(use-package evil-multiedit
  :after evil)

(use-package undo-tree
  :after evil)

(use-package evil-escape
  :commands evil-escape
  :hook (pre-command-hook . evil-escape)
  :init
  (setq evil-escape-excluded-states '(normal visual multiedit emacs motion)
        evil-escape-excluded-major-modes '(neotree-mode treemacs-mode vterm-mode)
        evil-escape-key-sequence "jk"
        evil-escape-delay 0.15)
  :config
  ;; no `evil-escape' in minibuffer
  (add-hook 'evil-escape-inhibit-functions #'minibufferp)
  ;; so that evil-escape-mode-hook runs, and can be toggled by evil-mc
  (evil-escape-mode +1))

(use-package evil-nerd-commenter
  :commands (evilnc-comment-operator
             evilnc-inner-comment
             evilnc-outer-commenter))


(use-package evil-snipe
  :commands (evil-snipe-mode
             evil-snipe-override-mode
             evil-snipe-local-mode
             evil-snipe-override-local-mode)
  :hook (pre-command-hook . evil-snipe)
  :init
  (setq evil-snipe-smart-case t
        evil-snipe-scope 'line
        evil-snipe-repeat-scope 'visible
        evil-snipe-char-fold t)
  :config
  (evil-snipe-mode +1)
  (evil-snipe-override-mode +1))


(use-package evil-surround
  :commands (global-evil-surround-mode
             evil-surround-edit
             evil-Surround-edit
             evil-surround-region)
  :config (global-evil-surround-mode 1))

;; Allows you to use the selection for * and #
(use-package evil-visualstar
  :commands (evil-visualstar/begin-search
             evil-visualstar/begin-search-forward
             evil-visualstar/begin-search-backward))

;;; Text object plugins

(use-package exato
  :commands evil-outer-xml-attr evil-inner-xml-attr)


(provide 'init-evil)

;;; init-evil ends here
