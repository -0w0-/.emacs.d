;; init-custom.el --- Define customizations.

;;; Commentary:
;;
;; Customizations.
;;

;;; Code:

(eval-when-compile
  (require 'init-const))

(defgroup Libra nil
  "Centaur Emacs customizations."
  :group 'convenience
  :link '(url-link :tag "Homepage" "https://bitbucket.org/-0w0-/.emacs.d/src/master/"))

(defcustom Libra-package-archives 'emacs-china
  "Set package archives from which to fetch."
  :group 'Libra
  :type '(choice
          (const :tag "Melpa" melpa)
          (const :tag "Melpa Mirror" melpa-mirror)
          (const :tag "Emacs-China" emacs-china)
          (const :tag "Netease" netease)
          (const :tag "Tencent" tencent)
          (const :tag "Tuna" tuna)))

(defcustom Libra-full-name "Yuhuai Zhou"
  "Set user full name."
  :group 'Libra
  :type 'string)

(defcustom Libra-mail-address "makefunny@outlook.com"
  "Set user email address."
  :group 'Libra
  :type 'string)

(defcustom Libra-tabbar nil
  "Set Libra—Emacs tabbar."
  :group 'Libra
  :type 'boolean)

;; Load `custom-file'
;; If it doesn't exist, copy from the template, then load it.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(let ((custom-template-file
       (expand-file-name "custom-template.el" user-emacs-directory)))
  (if (and (file-exists-p custom-template-file)
           (not (file-exists-p custom-file)))
      (copy-file custom-template-file custom-file)))

(if (file-exists-p custom-file)
    (load custom-file))

;; Load `custom-post.el'
;; Put personal configurations to override defaults here.
(add-hook 'after-init-hook
          (lambda ()
            (let ((file
                   (expand-file-name "custom-post.el" user-emacs-directory)))
              (if (file-exists-p file)
                  (load file)))))

(provide 'init-custom)

;;; init-custom.el ends here
