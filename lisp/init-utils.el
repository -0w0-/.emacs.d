;;; init-utils.el --- Initialize tools configurations.

;;; Commentary:
;;
;; Some usefule Utilities.
;;

;;; Code:

(use-package magit
  :bind
  ("C-x g" . magit-status))

;; Fast serach tool: `ripgrep'
(use-package rg
  :defines projectile-command-map
  :hook (after-init . rg-enable-default-bindings)
  :bind (:map rg-global-map
	      ("c" . rg-dwim-current-dir)
	      ("f" . rg-dwim-current-file)
	      ("m" . rg-menu)
	      :map rg-mode-map
	      ("m" . rg-menu))
  :init (setq rg-group-result t
	      rg-show-columns t)
  :config
  (cl-pushnew '("tmpl" . "*.tmpl") rg-custom-type-aliases)

  (with-eval-after-load 'projectile
    (defalias 'projectile-ripgrep #'rg-project)
    (bind-key "s R" #'rg-project projectile-command-map))

  (with-eval-after-load 'counsel
    (bind-keys
     :map rg-global-map
     ("R" . counsel-rg)
     ("F" . counsel-fzf))))

(when sys/linux-x-p
  (use-package telega
    :commands (telega)
    :defer t
    :load-path "~/.telega.el"
    :init
    (add-hook 'telega-chat-mode-hook #'evil-emacs-state)))

;; Youdao Example Key binding
(use-package youdao-dictionary
  :bind
  ("C-c y" . youdao-dictionary-search-at-point+))

(setq snails-path (expand-file-name "site-lisp/snails/snails.el" user-emacs-directory))

(if (file-exists-p snails-path)
    (use-package snails
      :load-path "site-lisp/snails"
      :hook (add-hook after-init-hook 'snails)
      :init
      (add-hook 'snails-mode-hook #'evil-emacs-state)))

(use-package restart-emacs)

(provide 'init-utils)

;;; init-utils.el ends here
