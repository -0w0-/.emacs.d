;; init-general.el --- Initialize general.

;;; Commentary:
;;
;; General * Global Keybind *
;;

;;; Code:

(eval-when-compile
  (require 'init-const))

;; Display available keybindings in popup
(use-package which-key
  :hook (after-init . which-key-mode)
  :bind (:map help-map ("C-h" . which-key-C-h-dispatch))
  :config
  ;; Allow C-h to trigger which-key before it is done automatically
  (setq which-key-show-early-on-C-h t)
  (setq which-key-idle-secondary-delay 0.05))

(use-package general
  :after init)

(general-define-key
 :states libra-input-states
 :keymaps 'dashboard-mode-map
 "q" 'quit-window)

(general-create-definer libra-leader-def
  :keymaps libra-input-states
  :non-normal-prefix libra-non-normal-leader
  :prefix libra-leader
  :prefix-command 'leader-def-command
  :prefix-map 'leader-prefix-map)

;; states to swiper
(libra-leader-def
  :keymaps libra-input-states
  "ESC" '(keyboard-quit :which-key "quit")
  "/" '(:which-key "search")
  "//" '(swiper :which-key "swiper")
  "/a" '(swiper-all :which-key "swiper-all"))

(libra-leader-def
  :states libra-input-states
  :keymaps 'csharp-mode-map
  "m" '(:which-key "omnisharp")
  "ms" '(omnisharp-start-omnisharp-server :which-key "omnisharp-server-start")
  "mc" '(recompile :which-key "recompile"))

(provide 'init-general)

;;; init-general.el ends here
