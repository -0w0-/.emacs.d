;;; init-csharp.el --- Csharp Configuration.

;;; Commentary:

;;; Code:

(defun libra-csharp-mode-hook()
  "Enable the stuff."
  (omnisharp-mode 1)
  (electric-pair-mode 1)
  (electric-pair-local-mode 1))

(use-package csharp-mode
  :hook
  (add-hook 'csharp-mode-hook
            'libra-csharp-mode-hook))

(use-package omnisharp)

(provide 'init-csharp)

;;; init-csharp.el ends here
