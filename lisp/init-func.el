;; init-funcs.el --- Define functions.

;;; Commentary:
;;
;; Define functions.
;;

;;; Code:

(define-minor-mode Libra-read-mode
  "Minor Mode for better reading experience."
  :init-value nil
  :group Libra
  (if Libra-read-mode
      (progn
        (when (fboundp 'olivetti-mode)
          (olivetti-mode 1))
        (when (fboundp 'mixed-pitch-mode)
          (mixed-pitch-mode 1)))
    (progn
      (when (fboundp 'olivetti-mode)
        (olivetti-mode -1))
      (when (fboundp 'mixed-pitch-mode)
        (mixed-pitch-mode -1)))))
(global-set-key (kbd "M-<f7>") #'Libra-read-mode)

(defun csharp-hs-forward-sexp (&optional arg)
  "csharp mode hideshow forward sexp."
  (message "csharp-hs-forward-sexp, (arg %d) (point %d)..."
           (if (numberp arg) arg -1)
           (point))
  
  (let ((nestlevel 0)
        (mark1 (point))
        (done nil)
        )
    
    (if (and arg (< arg 0))
        (message "negative arg (%d) is not supported..." arg)

      ;; else, we have a positive argument, hence move forward.
      ;; simple case is just move forward one brace
      (if (looking-at "{")
          (forward-sexp arg)
        
        ; The more complex case is dealing with a "region/endregion" block.
        ; We have to deal with nested regions!
        (and
         (while (not done)
           (re-search-forward "^[ \\t]*#[ \\t]*\\(region\\|endregion\\)\\b"
                              (point-max) 'move)
           (cond
            
            ((eobp))                    ; do nothing if at end of buffer
            
            ((and
              (match-beginning 1)
              ;; if the match is longer than 6 chars, we know it is "endregion"
              (if (> (- (match-end 1) (match-beginning 1)) 6)
                  (setq nestlevel (1- nestlevel))
                (setq nestlevel (1+ nestlevel))
                )
              )))

           (setq done (not (and (> nestlevel 0) (not (eobp)))))
           
           )                            ; while

         (if (= nest 0)
             (goto-char (match-end 2)))
         ) 
      )
    )
  )
)

;; UI
(defvar after-load-theme-hook nil
  "Hook run after a color theme is loaded using `load-theme'.")
(defun run-after-load-theme-hook(&rest _)
  "Run `after-load-theme-hook'."
  (run-hooks 'after-load-theme-hook))

;; Font
(defun font-installed-p (font-name)
  "Check if font with FONT-NAME is available."
  (find-font (font-spec :name font-name)))

(provide 'init-func)

;;; init-func.el ends here
