;;; init-ruby.el --- Initialize ruby configurations.

;;; Commentary:
;;
;; Ruby configurations.
;;

;;; Code:

(eval-when-compile
  (require 'init-custom))

(use-package ruby-mode
  :config
  ;; Ruby refactoring helpers
  (use-package ruby-refactor
    :diminish
    :hook (ruby-mode . ruby-refactor-mode-launch))

  ;; Run a Ruby process in buffer
  (use-package inf-ruby
    :hook ((ruby-mode . inf-ruby-minor-mode)
	   (compilation-filter . inf-ruby-auto-enter)))

  ;; Rubocop
  ;; Install: gem nstall rubocop
  (use-package rubocop
    :diminish
    :hook (ruby-mode . rubocop-mode))

  ;; Rspec
  (use-package rspec-mode
    :diminish
    :commands rspec-install-snippets
    :hook (dired-mode . rspec-dired-mode)
    :config (with-eval-after-load 'yasnippet
	      (rspec-install-snippets)))

  ;; Yet Another RI Interface for Emacs
  (use-package yari
    :bind (:map ruby-mode-map ([f1] . yari)))

  ;; Ruby YARD comments
  (use-package yard-mode
    :diminish
    :hook (ruby-mode . yard-mode)))

;; YAML Mode
(use-package yaml-mode)

(provide 'init-ruby)

;;; init-ruby.el ends here
